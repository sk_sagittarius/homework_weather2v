﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HomeworkWeather2v
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void SearchButtonClick(object sender, RoutedEventArgs e)
        {
            var date = DateTime.Now;
            var cityWeather = new GetWeather();
            var city = cityWeather.GetWeatherFunction(textBoxSearch.Text.ToLower());
            var cityLocation = city.Location;
            var cityCurrent = city.Current;
            var cityForecast = city.Forecast.Forecastday;

            #region City name
            tBlockCityNameFirstDay.Text = cityLocation.Name + ", " + cityLocation.Country;
            tBlockCityNameSecondDay.Text = cityLocation.Name + ", " + cityLocation.Country;
            tBlockCityNameThirdDay.Text = cityLocation.Name + ", " + cityLocation.Country;
            tBlockCityNameFourthDay.Text = cityLocation.Name + ", " + cityLocation.Country;
            tBlockCityNameFifthDay.Text = cityLocation.Name + ", " + cityLocation.Country;
            tBlockCityNameSixthDay.Text = cityLocation.Name + ", " + cityLocation.Country;
            tBlockCityNameSeventhDay.Text = cityLocation.Name + ", " + cityLocation.Country;
            #endregion

            #region Date
            tBlockDateFirstDay.Text = cityForecast.ElementAt(0).Date.ToString("dd-MM-yyyy");
            tBlockDateSecondDay.Text = cityForecast.ElementAt(1).Date.ToString("dd-MM-yyyy");
            tBlockDateThirdDay.Text = cityForecast.ElementAt(2).Date.ToString("dd-MM-yyyy");
            tBlockDateFourthDay.Text = cityForecast.ElementAt(3).Date.ToString("dd-MM-yyyy");
            tBlockDateFifthDay.Text = cityForecast.ElementAt(4).Date.ToString("dd-MM-yyyy");
            tBlockDateSixthDay.Text = cityForecast.ElementAt(5).Date.ToString("dd-MM-yyyy");
            tBlockDateSeventhDay.Text = cityForecast.ElementAt(6).Date.ToString("dd-MM-yyyy");
            #endregion

            #region isDay & logo
            string isDay = "";
            if (cityCurrent.IsDay == 1)
            {
                isDay = "Day";
            }
            else
            { isDay = "Night"; }

            BitmapImage logo0 = new BitmapImage();
            BitmapImage logo1 = new BitmapImage();
            BitmapImage logo2 = new BitmapImage();
            BitmapImage logo3 = new BitmapImage();
            BitmapImage logo4 = new BitmapImage();
            BitmapImage logo5 = new BitmapImage();
            BitmapImage logo6 = new BitmapImage();
            
            #endregion

            #region Weather information
            tBlockCityInfoFirstDay.Text = isDay + "\nSunrise: " + cityForecast.ElementAt(0).Astro.Sunrise
                + "\nSunset: " + cityForecast.ElementAt(0).Astro.Sunset + "\nTemperature: " + cityCurrent.TempC + "\nWind: " + cityCurrent.WindKph + "\nUV-index: " + cityCurrent.Uv;
            tBlockCityInfoSecondDay.Text = isDay + "\nSunrise: " + cityForecast.ElementAt(1).Astro.Sunrise
                + "\nSunset: " + cityForecast.ElementAt(1).Astro.Sunset + "\nTemperature: " + cityForecast.ElementAt(1).Day.AvgtempC
                + "\nWind: " + cityForecast.ElementAt(1).Day.MaxwindKph + "\nUV-index: " + cityForecast.ElementAt(1).Day.Uv;
            tBlockCityInfoThirdDay.Text = isDay + "\nSunrise: " + cityForecast.ElementAt(2).Astro.Sunrise
                + "\nSunset: " + cityForecast.ElementAt(2).Astro.Sunset + "\nTemperature: " + cityForecast.ElementAt(2).Day.AvgtempC
                + "\nWind: " + cityForecast.ElementAt(2).Day.MaxwindKph + "\nUV-index: " + cityForecast.ElementAt(2).Day.Uv;
            tBlockCityInfoFourthDay.Text = isDay + "\nSunrise: " + cityForecast.ElementAt(3).Astro.Sunrise
                + "\nSunset: " + cityForecast.ElementAt(3).Astro.Sunset + "\nTemperature: " + cityForecast.ElementAt(3).Day.AvgtempC
                + "\nWind: " + cityForecast.ElementAt(3).Day.MaxwindKph + "\nUV-index: " + cityForecast.ElementAt(3).Day.Uv;
            tBlockCityInfoFifthDay.Text = isDay + "\nSunrise: " + cityForecast.ElementAt(4).Astro.Sunrise
                + "\nSunset: " + cityForecast.ElementAt(4).Astro.Sunset + "\nTemperature: " + cityForecast.ElementAt(4).Day.AvgtempC
                + "\nWind: " + cityForecast.ElementAt(4).Day.MaxwindKph + "\nUV-index: " + cityForecast.ElementAt(4).Day.Uv;
            tBlockCityInfoSixthDay.Text = isDay + "\nSunrise: " + cityForecast.ElementAt(5).Astro.Sunrise
                + "\nSunset: " + cityForecast.ElementAt(5).Astro.Sunset + "\nTemperature: " + cityForecast.ElementAt(5).Day.AvgtempC
                + "\nWind: " + cityForecast.ElementAt(5).Day.MaxwindKph + "\nUV-index: " + cityForecast.ElementAt(5).Day.Uv;
            tBlockCityInfoSeventhDay.Text = isDay + "\nSunrise: " + cityForecast.ElementAt(6).Astro.Sunrise
                + "\nSunset: " + cityForecast.ElementAt(6).Astro.Sunset + "\nTemperature: " + cityForecast.ElementAt(6).Day.AvgtempC
                + "\nWind: " + cityForecast.ElementAt(6).Day.MaxwindKph + "\nUV-index: " + cityForecast.ElementAt(6).Day.Uv;
            #endregion

            #region Condition
            logo0.BeginInit();
            logo0.UriSource = new Uri("https:" + cityCurrent.Condition.Icon);
            logo0.EndInit();

            logo1.BeginInit();
            logo1.UriSource = new Uri("https:" + cityForecast.ElementAt(1).Day.Condition.Icon);
            logo1.EndInit();

            logo2.BeginInit();
            logo2.UriSource = new Uri("https:" + cityForecast.ElementAt(2).Day.Condition.Icon);
            logo2.EndInit();

            logo3.BeginInit();
            logo3.UriSource = new Uri("https:" + cityForecast.ElementAt(3).Day.Condition.Icon);
            logo3.EndInit();

            logo4.BeginInit();
            logo4.UriSource = new Uri("https:" + cityForecast.ElementAt(4).Day.Condition.Icon);
            logo4.EndInit();

            logo5.BeginInit();
            logo5.UriSource = new Uri("https:" + cityForecast.ElementAt(5).Day.Condition.Icon);
            logo5.EndInit();

            logo6.BeginInit();
            logo6.UriSource = new Uri("https:" + cityForecast.ElementAt(6).Day.Condition.Icon);
            logo6.EndInit();

            sPImageFirstDay.Source = logo0;
            sPImageSecondDay.Source = logo1;
            sPImageThirdDay.Source = logo2;
            sPImageFourthDay.Source = logo3;
            sPImageFifthDay.Source = logo4;
            sPImageSixthDay.Source = logo5;
            sPImageSeventhDay.Source = logo6;

            sPTBlockConditionFirstDay.Text = cityCurrent.Condition.Text;
            sPTBlockConditionSecondDay.Text = cityForecast.ElementAt(1).Day.Condition.Text;
            sPTBlockConditionThirdDay.Text = cityForecast.ElementAt(2).Day.Condition.Text;
            sPTBlockConditionFourthDay.Text = cityForecast.ElementAt(3).Day.Condition.Text;
            sPTBlockConditionFifthDay.Text = cityForecast.ElementAt(4).Day.Condition.Text;
            sPTBlockConditionSixthDay.Text = cityForecast.ElementAt(5).Day.Condition.Text;
            sPTBlockConditionSeventhDay.Text = cityForecast.ElementAt(6).Day.Condition.Text;
            #endregion

            #region Image
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.UriSource = new Uri("https://www.fox19.com/pb/resources/images/weather/weather-condition-icons/400x400/70_daily_forecast.png");
            image.EndInit();
            justImage.Source = image;
            justImage2.Source = image;
            #endregion

        }
    }
}
