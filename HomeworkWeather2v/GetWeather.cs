﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkWeather2v
{
    public class GetWeather
    {
        public CityWeather GetWeatherFunction(string cityName)
        {

            using (WebClient client = new WebClient())
            {
                if (cityName == "")
                {
                    var jsonString = client.DownloadString($"http://api.apixu.com/v1/forecast.json?key=daf5182443d94f5d88274907192505&q=astana&days=7");
                    var cityWeather = CityWeather.FromJson(jsonString);
                    return cityWeather;
                }
                else
                {
                    var jsonString = client.DownloadString($"http://api.apixu.com/v1/forecast.json?key=daf5182443d94f5d88274907192505&q={cityName}&days=7");
                    var cityWeather = CityWeather.FromJson(jsonString);
                    return cityWeather;
                }
            }
        }
    }
}
